<?php

namespace Bridge\Core\Components;

use dosamigos\grid\columns\ToggleColumn;
use Yii;

class RFAToggleColumn extends ToggleColumn
{
    public $onValue = 1;
    public $onLabel;
    public $offLabel;
    public $contentOptions = ['class' => 'col-md-1 text-center'];
    public $headerOptions = ['class' => 'col-md-1'];
    public $onIcon = 'glyphicon glyphicon-ok-circle btn btn-success';
    public $offIcon = 'glyphicon glyphicon-remove-circle btn btn-default';
    public $filter;

    public function __construct($config = [])
    {
        $this->onLabel = Yii::t('app','Да');
        $this->offLabel = Yii::t('app','Нет');
        $this->filter = [
            1 => Yii::t('app','Активный'),
            0 => Yii::t('app','Не активный'),
        ];
        parent::__construct($config);
    }
}
