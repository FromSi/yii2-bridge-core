<?php

namespace Bridge\Core\Assets;

/**
 * Класс помогает настроить палитру цветов всей админки (beta)
 *
 * Class BridgeTheme
 * @package Bridge\Core\Assets
 */
class BridgeTheme
{
    /**
     * @var string Первичная шапка (меню)
     */
    public $firstHeader = '#55CBEE';

    /**
     * @var string Вторичная шапка (меню)
     */
    public $lastHeader = '#2F7ADB';

    /**
     * @var string Параметры линейного градиента (меню)
     */
    public $linearGradientHeader = 'to bottom right';

    /**
     * @var string Первичный фон (меню)
     */
    public $firstBackground = '#323461';

    /**
     * @var string Вторичный фон (меню, язык, кнопки primary, переключатель (истина))
     */
    public $lastBackground = '#434392';

    /**
     * @var string Первичная активность (меню)
     */
    public $firstActive = '#EF476F';

    /**
     * @var string Вторичная активность (меню)
     */
    public $lastActive = '#CA3D5E';

    /**
     * @var string Первичный передний план (шрифт для темных и выбранных элементов)
     */
    public $firstForeground = '#DBE4EE';

    /**
     * @var string Вторичный передний план (шрифт для темных и выбранных элементов)
     */
    public $lastForeground = '#F5F6FB';

    /**
     * @var string Наводка курсором (меню и язык)
     */
    public $hover = '#2B2D5D';

    /**
     * @var string Наводка курсором для danger.
     */
    public $hoverDanger = '#D9534F';

    /**
     * @var string Наводка курсором для success.
     */
    public $hoverSuccess = '#449D44';

    /**
     * @var string Наводка курсором для info.
     */
    public $hoverInfo = '#31B0D5';

    /**
     * @var string Наводка курсором для warning.
     */
    public $hoverWarning = '#EC971F';

    /**
     * @var string Ссылки по которым можно нажать
     */
    public $link = '#5353B5';

    /**
     * @var string Для опасных элементов (кнопка выхода, удаления и переключатель (ложь))
     */
    public $danger = '#EF476F';

    /**
     * @var string Для положительных элементов (кнопка создания, активности и редактирования (истина))
     */
    public $success = '#5CB85C';

    /**
     * @var string Для информативных элементов (кнопка просмотра, позиции)
     */
    public $info = '#5BC0DE';

    /**
     * @var string Для предупреждающих элементов (кнопка очистки кэша)
     */
    public $warning = '#F0AD4E';

    /**
     * @return string готовый css
     */
    public function css(){
        return ":root {
            --first-header: {$this->firstHeader};
            --last-header: {$this->lastHeader};
            --linear-gradient-header: {$this->linearGradientHeader};
            --first-background: {$this->firstBackground};
            --last-background: {$this->lastBackground};
            --first-active: {$this->firstActive };
            --last-active: {$this->lastActive};
            --first-foreground: {$this->firstForeground};
            --last-foreground: {$this->lastForeground};
            --hover: {$this->hover};
            --hover-danger: {$this->hoverDanger};
            --hover-success: {$this->hoverSuccess};
            --hover-info: {$this->hoverInfo};
            --hover-warning: {$this->hoverWarning};
            --link: {$this->link};
            --danger: {$this->danger};
            --success: {$this->success};
            --info: {$this->info};
            --warning: {$this->warning};
        }";
    }
}
